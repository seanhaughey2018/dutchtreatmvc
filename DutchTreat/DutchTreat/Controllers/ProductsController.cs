﻿using DutchTreat.data;
using DutchTreat.data.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DutchTreat.Controllers
{
    [Route("api/[Controller]")]
    public class ProductsController:Controller
    {
        private readonly IDutchRepository _repository;

        public ProductsController(IDutchRepository repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
              return Ok(_repository.GetAllProducts());
            }
            catch(Exception ex)
            {
                return BadRequest("Failed to get products");
            }
            
        }
    }
}
