﻿using DutchTreat.data;
using DutchTreat.data.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DutchTreat.Controllers
{
    [Route("api/[Controller]")]
    public class OrdersController:Controller
    {
        private readonly IDutchRepository _repository;

        public OrdersController(IDutchRepository repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return Ok(_repository.GetAllOrders());
            }
            catch (Exception ex)
            {
                return BadRequest("Failed to get products");
            }
        }

        [HttpGet("{id:int}")]
        public IActionResult Get(int id)
        {
            try
            {
                var order = _repository.GetOrderById(id);
                if (order != null)
                    return Ok(order);
                else
                    return NotFound();

            }
            catch (Exception ex)
            {
                return BadRequest("Failed to get products");
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody]Order Model)  //frombody gets data
        {
            try
            {
                _repository.AddEntity(Model);
               // _repository.SaveChanges(); //change to save all

                if (_repository.SaveChanges())
                {
                    return Created($"/api/orders/{Model.Id}", Model);  //use created on a post
                }
               
            }
            catch (Exception ex)
            {
                return BadRequest("Failed to save");
            }
            return BadRequest("Failed to save");
        }
    }
}
