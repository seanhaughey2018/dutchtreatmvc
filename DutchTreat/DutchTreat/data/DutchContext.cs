﻿using DutchTreat.data.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DutchTreat.data
{
    public class DutchContext:DbContext
    {
        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Order { get; set; }

        public DutchContext(DbContextOptions<DutchContext> options):base(options)
        {
        }
    }
}
