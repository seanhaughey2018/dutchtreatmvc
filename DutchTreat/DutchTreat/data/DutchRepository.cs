﻿using DutchTreat.data.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DutchTreat.data
{
    public class DutchRepository:IDutchRepository
    {
        private readonly DutchContext _ctx;

        public DutchRepository(DutchContext ctx)
        {
            _ctx = ctx;
        }

        public void AddEntity(object model)
        {
            _ctx.Add(model);
        }

        public IEnumerable<Order> GetAllOrders()
        {
            return _ctx.Order
                .Include(o=>o.Items)
                .ThenInclude(i=>i.Product)
                .ToList();
        }

        public IEnumerable<Product> GetAllProducts()
        {
            return _ctx.Products.OrderBy(p => p.Title).ToList();

        }

        public Order GetOrderById(int id)
        {
            return _ctx.Order
              .Include(o => o.Items)
              .ThenInclude(i => i.Product)
              .Where(o=>o.Id ==id)
              .FirstOrDefault();
        }

        public IEnumerable<Product>GetProductsByCategory(string category)
        {
            return _ctx.Products.Where(c => c.Category == category).ToList();
        }

        public bool SaveChanges()
        {
            return _ctx.SaveChanges() > 0;
        }
    }
}
